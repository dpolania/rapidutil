import RapidUtil

/// Convierte un numero a formato moneda
let currency = FormatCurrency()
let transform = currency.convertNumberCurrency(number: 10000)
PrintInfo("\(transform)", "convertNumberCurrency")

/// Convierte el formato moneda en un Double
let doubleTransform = currency.convertCurrencyDouble(stringNumber: "$1,000,000")
PrintInfo("\(doubleTransform)", "doubleTransform")


/// Convierte string a formato moneda
let chartsConvert = currency.maskByCharacters(text: "10.456")
PrintInfo("\(chartsConvert)")


///Util for Date

/// Convierte la fecha en un string de acuerdo al formato ingresado
let dateUtil = DateUtil()
let dateResult = dateUtil.convertDateString(date: Date(),format: "d/MM/yyyy")
PrintInfo("\(dateResult)", "dateResult")

/// Convierte un String en un Date
let stringResultDate = dateUtil.convertStringDate(date: "02/02/2021",format: "d/MM/yyyy")
PrintInfo("\(stringResultDate)", "stringResultDate")

// Crea un Date ingresando cada uno de los componentes
let createDate = dateUtil.createDate(day: 1, mounth: 12, year: 2021, hour: 12, minute: 5)
PrintInfo("\(createDate)", "createDate",.warning)

// Adicion del componente a la fecha deceada
let addingDate = dateUtil.addingDate(date: Date(), componentDate: .year, value: 2)
PrintInfo("\(addingDate)", "addingDate")

//Se encarga de mostrar diferencia entre las fecha
let rangeDate = dateUtil.getRangeBetwenYears(firstYear: Date(), lastYear: addingDate,componentDate: .minute)
PrintInfo("\(rangeDate)", "rangeDate",.danger)
