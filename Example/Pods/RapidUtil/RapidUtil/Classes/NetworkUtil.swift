import UIKit

/**
    enumerador para determinar el metodo del consumo del api rest
 */
public enum httpMethod : String {
    case post   = "POST"
    case get    = "GET"
    case put    = "PUT"
    case delete = "DELETE"
}

public class networUtil{
    public init(){
    }
    public func requestAPI(url service  : String,
                    body : [String:Any] = [:],
                    header : [String:String] = [:],
                    method : httpMethod = .get,
                    queue  : DispatchQueue = DispatchQueue.init(label: "backService", qos: .background, attributes: .concurrent),
                    completion: @escaping ([String: Any]?, Error?) -> Void){

        let url = URL(string: service)!
        let session = URLSession.shared


        var request = URLRequest(url: url)
        request.timeoutInterval = TimeInterval(1000)
        request.httpMethod = method.rawValue
        
        if body.count > 0 {
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: body, options: .prettyPrinted) // pass dictionary to data object and set it as request body
            } catch let error {
                print(error.localizedDescription)
                completion(nil, error)
            }
        }
        
        if header.count > 0 {
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            header.forEach{
                request.addValue($0.0, forHTTPHeaderField: $0.1)
            }
        }
        queue.async {
            let task = session.dataTask(with: request, completionHandler: { data, response, error in

                guard error == nil else {
                    completion(nil, error)
                    return
                }

                guard let data = data else {
                    completion(nil, NSError(domain: "dataNilError", code: -100001, userInfo: nil))
                    return
                }

                do {
                    //create json object from data
                    guard let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] else {
                        completion(nil, NSError(domain: "invalidJSONTypeError", code: -100009, userInfo: nil))
                        return
                    }
                    completion(json, nil)
                } catch let error {
                    completion(nil, error)
                }
            })

            task.resume()
        }
    }
}
