//
//  NavigationUtil.swift
//  Pods
//
//  Created by David Camilo Polania Losada on 23/12/21.
//

import Foundation
import UIKit

extension UIViewController{

    public func pushView(_ ruteNavigation : UIViewController){
        let controller = ruteNavigation
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    public func presentView(_ controller : UIViewController,completion: (() -> Void)? = nil, styleModal : UIModalPresentationStyle){
        controller.modalPresentationStyle = styleModal
        self.navigationController?.present(controller, animated: true, completion: completion)
    }
    
    public func popView(controller : UIViewController){
        for view in navigationController?.viewControllers ?? [] {
            if view.isKind(of: controller.classForCoder) {
                self.navigationController?.popToViewController(view, animated: true)
            }
        }
    }
}
