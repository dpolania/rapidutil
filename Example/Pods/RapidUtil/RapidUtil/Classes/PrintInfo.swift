import Foundation

public enum PrintStatus:String{
    case danger  = "🔴"
    case warning = "🟠"
    case info    = "🔵"
}


public struct PrintInfo {
    var date : Date = Date()
    private static var Id = 1
    
    public init(_ descripcion : Any? = nil, _ title : String = "Debug",_ status : PrintStatus = PrintStatus.info) {
        print("______________________________________________________")
        print("\(status.rawValue) \(title)--\(self.date.timeIntervalSince1970)")
        print("______________________________________________________")
        print("\(String(describing: descripcion ?? ""))")
        print("______________________________________________________\n")
    }
    
}
