import Foundation
import UIKit

public class FormatCurrency{
    
    var locale : Locale?
    public init(_ locale : Locale = Locale.current) {
        self.locale = locale
    }
    /**
     Converts a double number to a currency format, this function receives as a parameter the
     location and the number of decimal places, if these are not specified by default the location
     is from the system and the number of decimal places is 2
     */
    public func convertNumberCurrency(number : Double, _ maxDecimal : Int = 2) -> String{
        let myDouble = number
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = .currency
        ///Separador por comas o putos depende del la localizacion actual
        currencyFormatter.locale = self.locale
        currencyFormatter.maximumFractionDigits = maxDecimal


        let priceString = currencyFormatter.string(from: NSNumber(value: myDouble))!
        return priceString
    }
    
    /**
     Converts the currency format to a double
     */
    public func convertCurrencyDouble(stringNumber : String) -> Double{
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.locale = self.locale

        if let number = formatter.number(from: stringNumber) {
            let amount = number.doubleValue
            return amount
        }else{
            return 0.00
        }
    }
    
    /**
     in mask a double character by character ideal to use in a text input
     */
    public func maskByCharacters(text : String) -> String{
        let formatter =  NumberFormatter()
        let separator = formatter.decimalSeparator ?? "."
        let number = text.components(separatedBy: separator)
        formatter.numberStyle = .currency
        formatter.locale = self.locale
        let maxDecimal = number.count > 1 ? number.last?.count : 0
        
        if let numberCurrency = Double(text), text.last != "." {
            return convertNumberCurrency(number: numberCurrency, maxDecimal ?? 0)
        }else{
            if text.contains(separator){
                if text.last == "." {
                    return "\(maskByCharacters(text : number.first ?? "0"))."
                }else{
                    return maskByCharacters(text : text)
                }
            }else{
                return ""
            }
        }
    }
}
