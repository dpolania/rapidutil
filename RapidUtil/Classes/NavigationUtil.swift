//
//  NavigationUtil.swift
//  Pods
//
//  Created by David Camilo Polania Losada on 23/12/21.
//

import Foundation
import UIKit

extension UIViewController{

    public func pushView(_ ruteNavigation : Flow){
        let controller = navigationFlowViewController(flow: ruteNavigation)
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    public func presentView(_ ruteNavigation : Flow,completion: (() -> Void)? = nil, styleModal : UIModalPresentationStyle){
        let controller = navigationFlowViewController(flow: ruteNavigation)
        controller.modalPresentationStyle = styleModal
        self.present(controller, animated: true, completion: completion)
    }
    
    public func popView(controller : UIViewController){
        for view in navigationController?.viewControllers ?? [] {
            if view.isKind(of: controller.classForCoder) {
                self.navigationController?.popToViewController(view, animated: true)
            }
        }
    }
    
    private func navigationFlowViewController(flow : Flow) -> UIViewController{
        return flow.getViewController()
    }
}
