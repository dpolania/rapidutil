import Foundation

public class DateUtil{
    var locale : Locale?
    let calendar = NSCalendar.current
    
    public init(_ locale : Locale = Locale.current) {
        self.locale = locale
    }
    
    public func convertStringDate(date:String, format : String = "YY/MM/dd") -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.locale = locale
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from:date)!
        return date
    }
    public func convertDateString(date:Date,format : String = "YY/MM/dd") -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    
    public func createDate(day : Int,mounth : Int, year : Int, hour : Int, minute : Int)->Date{
        let components = NSDateComponents()
        
        components.day = day
        components.month = mounth
        components.year = year
        components.hour = hour
        components.minute = minute
        return calendar.date(from: components as DateComponents) ?? Date()
        
    }
    
    public func addingDate(date:Date,componentDate:Calendar.Component, value : Int)->Date{
        return calendar.date(byAdding: componentDate, value: value, to: date) ?? Date()
    }
    
    public func getRangeBetwenYears(firstYear: Date, lastYear: Date,componentDate:Calendar.Component = .day) -> Int {
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([componentDate], from: firstYear, to: lastYear)
        return ageComponents.value(for: componentDate) ?? 0
    }
    
    public func getUserAge(birthday:Date) -> Int {
        let now = Date()
        let calendar = Calendar.current
        let ageComponents = calendar.dateComponents([.year], from: birthday, to: now)
        let age = ageComponents.year ?? 0
        return age
    }
    
    public func dateMinYearsBeforeDate(menorDate: Date, mayorDate: Date, minYears: Int) -> Bool {
        let calendar = Calendar.current
        
        // Extract the components of the dates
        let date1Components = calendar.dateComponents([.year], from: menorDate)
        let date2Components = calendar.dateComponents([.year], from: mayorDate)
        
        
        let yearsDifference = date2Components.year! - date1Components.year!
        
        return yearsDifference >= minYears
    }
}
