#
# Be sure to run `pod lib lint RapidUtil.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'RapidUtil'
  s.version          = '1.0.4'
  s.summary          = 'This is a library that allows the faster development of projects since it contains numerous utilities'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'This is a library that allows the faster development of projects since it contains numerous utilities, these allow us to manage dates, currency format, navigation and http communication among other functionalities. Based on this, an attempt is made to have a collection of the most used functions by developers.'
  s.homepage         = 'https://gitlab.com/dpolania/rapidutil.git'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'David Camilo Polania Losada' => 'dpolania@outlook.es' }
  s.source           = { :git => 'https://gitlab.com/dpolania/rapidutil.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'RapidUtil/Classes/**/*'
  
  s.swift_version = '4.1'
  
  # s.resource_bundles = {
  #   'RapidUtil' => ['RapidUtil/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
