# RapidUtil

[![CI Status](https://img.shields.io/travis/David Camilo Polania Losada/RapidUtil.svg?style=flat)](https://travis-ci.org/David Camilo Polania Losada/RapidUtil)
[![Version](https://img.shields.io/cocoapods/v/RapidUtil.svg?style=flat)](https://cocoapods.org/pods/RapidUtil)
[![License](https://img.shields.io/cocoapods/l/RapidUtil.svg?style=flat)](https://cocoapods.org/pods/RapidUtil)
[![Platform](https://img.shields.io/cocoapods/p/RapidUtil.svg?style=flat)](https://cocoapods.org/pods/RapidUtil)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

RapidUtil is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'RapidUtil'
```

## Author

David Camilo Polania Losada, david.polania@valid.com

## License

RapidUtil is available under the MIT license. See the LICENSE file for more info.
